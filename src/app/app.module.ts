import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { JwtModule } from '@auth0/angular-jwt';
import { CoreModule } from './modules/core/core.module';
import { DragonsModule } from './modules/dragons/dragons.module';

export function getJWTtoken() {
  return localStorage.getItem('token');
}

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    CoreModule,
    DragonsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    // Configuration for jwt injector
    // Due to the open API there's no need to config this module
    JwtModule.forRoot({
      config: {
        tokenGetter: getJWTtoken,
        whitelistedDomains: [''],
        blacklistedRoutes: ['']
      }
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
