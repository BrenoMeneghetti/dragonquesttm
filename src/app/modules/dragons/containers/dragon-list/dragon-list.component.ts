import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Dragon } from 'src/app/shared/models/dragon';
import { DragonService } from '../../services/dragon.service';
import { AlertService } from 'src/app/modules/core/services/alert.service';

@Component({
  selector: 'app-dragon-list',
  templateUrl: './dragon-list.component.html',
  styleUrls: ['./dragon-list.component.scss']
})
export class DragonListComponent implements OnInit {
  dragonList: Dragon[];
  constructor(
    private route: ActivatedRoute,
    private dragonService: DragonService,
    private router: Router,
    private alertService: AlertService
  ) {}

  ngOnInit() {
    this.route.data.subscribe(data => {
      this.dragonList = data.dragonList;
    });
  }

  openDragon(id: number) {
    this.router.navigate([`/dragon/${id}`]);
  }

  deleteDragon(dragon: Dragon) {
    const dialogConfig = {
      title: `Confirm Delete`,
      description: `Do you really want delete dragon ${dragon.name} ?`
    };

    this.alertService
      .openConfirmDialog(dialogConfig.title, dialogConfig.description)
      .subscribe(result => {
        if (result) {
          this.dragonService.deleteDragon(dragon.id).subscribe(
            () => {
              this.alertService.openSnackBar(
                'Dragon deleted with success.',
                'Close'
              );
              this.dragonList = this.dragonList.filter(
                listItem => listItem.id !== dragon.id
              );
            },
            error => {
              this.alertService.openSnackBar(
                'Delete operation has failed.',
                'Close'
              );
            }
          );
        }
      });
  }

  editDragon(id: number) {
    this.router.navigate([`/dragon/edit/${id}`]);
  }
}
