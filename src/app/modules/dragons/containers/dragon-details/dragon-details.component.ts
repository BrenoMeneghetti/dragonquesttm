import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Dragon } from 'src/app/shared/models/dragon';

@Component({
  selector: 'app-dragon-details',
  templateUrl: './dragon-details.component.html',
  styleUrls: ['./dragon-details.component.scss']
})
export class DragonDetailsComponent implements OnInit {
  dragon: Dragon;
  constructor(private route: ActivatedRoute, private router: Router) {
    this.route.data.subscribe(data => {
      this.dragon = data.dragon;
    });
  }

  ngOnInit() {}

  editDragon() {
    this.router.navigate([`/dragon/edit/${this.dragon.id}`]);
  }
}
