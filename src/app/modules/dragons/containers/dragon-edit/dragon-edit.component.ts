import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Dragon } from 'src/app/shared/models/dragon';
import { DragonService } from '../../services/dragon.service';
import { AlertService } from 'src/app/modules/core/services/alert.service';
import { DragonFormComponent } from '../../components/dragon-form/dragon-form.component';

@Component({
  selector: 'app-dragon-edit',
  templateUrl: './dragon-edit.component.html',
  styleUrls: ['./dragon-edit.component.scss']
})
export class DragonEditComponent implements OnInit {
  @ViewChild('dragonForm', {static: true}) dragonForm: DragonFormComponent;
  dragon: Dragon;
  requestInProgress = false;
  constructor(
    private route: ActivatedRoute,
    private dragonService: DragonService,
    private alertService: AlertService
  ) {}

  ngOnInit() {
    this.route.data.subscribe(data => {
      this.dragon = data.dragon;
    });
  }

  updateDragon(dragon: Dragon) {
    const modifiedDragon = Object.assign({}, this.dragon, dragon);
    this.requestInProgress = true;
    this.dragonService.editDragon(this.dragon.id, modifiedDragon).subscribe(
      (response: Dragon) => {
        this.requestInProgress = false;
        this.alertService.openSnackBar('Dragon modified with success.', 'Close');
        this.dragon = response;
      },
      error => {
        this.requestInProgress = false;
        this.alertService.openSnackBar('Dragon creation has failed.', 'Close');
      }
    );
  }
}
