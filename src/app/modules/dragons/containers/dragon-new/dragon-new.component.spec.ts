import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DragonNewComponent } from './dragon-new.component';

describe('DragonNewComponent', () => {
  let component: DragonNewComponent;
  let fixture: ComponentFixture<DragonNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DragonNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DragonNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
