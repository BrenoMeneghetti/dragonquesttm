import { Component, OnInit, ViewChild } from '@angular/core';
import { DragonService } from '../../services/dragon.service';
import { Dragon } from 'src/app/shared/models/dragon';
import { Router } from '@angular/router';
import { AlertService } from 'src/app/modules/core/services/alert.service';
import { DragonFormComponent } from '../../components/dragon-form/dragon-form.component';

@Component({
  selector: 'app-dragon-new',
  templateUrl: './dragon-new.component.html',
  styleUrls: ['./dragon-new.component.scss']
})
export class DragonNewComponent implements OnInit {
  @ViewChild('dragonForm', {static: true}) dragonForm: DragonFormComponent;
  requestInProgress = false;
  constructor(
    private dragonService: DragonService,
    private router: Router,
    private alertService: AlertService
  ) {}

  ngOnInit() {}

  saveDragon(formValue: Dragon) {
    this.requestInProgress = true;
    this.dragonService.createDragon(formValue).subscribe(
      value => {
        this.requestInProgress = false;
        this.alertService.openSnackBar('Dragon created with success.', 'Close');
        this.router.navigate([`dragons`]);
      },
      error => {
        this.requestInProgress = false;
        this.alertService.openSnackBar('Dragon creation has failed.', 'Close');
      }
    );
  }
}
