import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { dragonRoutes } from './routes';

@NgModule({
  imports: [RouterModule.forChild(dragonRoutes)],
  exports: [RouterModule]
})
export class DragonsRoutingModule { }
