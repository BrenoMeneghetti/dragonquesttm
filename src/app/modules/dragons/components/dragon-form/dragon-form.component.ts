import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Dragon } from 'src/app/shared/models/dragon';

@Component({
  selector: 'app-dragon-form',
  templateUrl: './dragon-form.component.html',
  styleUrls: ['./dragon-form.component.scss']
})
export class DragonFormComponent implements OnInit {
  initialValue: Dragon;
  @Input() loading: boolean;
  @Input() set dragonInformation(value: Dragon) {
    if (value) {
      this.initialValue = value;
      this.dragonForm.patchValue(value);
    }
  }
  @Output() submitEvent = new EventEmitter<Dragon>();

  dragonForm = null;
  constructor(private fb: FormBuilder) {
    this.dragonForm = this.fb.group(
      {
        name: [null, Validators.required],
        type: [null, Validators.required]
      },
      {
        validator: this.valueHasChangedValidator.bind(this)
      }
    );
  }

  ngOnInit() {}

  onSubmit() {
    this.submitEvent.emit(this.dragonForm.value);
  }

  resetForm() {
    this.dragonForm.patchValue(this.initialValue);
  }

  valueHasChangedValidator(g: FormGroup) {
    return !this.initialValue ||
      g.get('name').value !== this.initialValue.name ||
      g.get('type').value !== this.initialValue.type
      ? null
      : { unchanged: true };
  }
}
