import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Dragon } from 'src/app/shared/models/dragon';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  private _dragonList: Dragon[];
  @Input() set dragonList(value: Dragon[]) {
    this._dragonList = value;
    this.filterList('');
  }
  get dragonList() {
    return this._dragonList;
  }
  filteredList: Dragon[];

  @Output() editDragon = new EventEmitter<number>();
  @Output() deleteDragon = new EventEmitter<Dragon>();
  @Output() openDragon = new EventEmitter<number>();

  constructor() {}

  ngOnInit() {}

  filterList(filterText: string) {
    this.filteredList = this.dragonList.filter(
      dragon =>
        dragon.name.indexOf(filterText) > -1 ||
        dragon.type.indexOf(filterText) > -1
    );

    this.filteredList.sort(this.sortAlphabetically);
  }

  sortAlphabetically(a, b) {
    return a.name.toLowerCase().localeCompare(b.name.toLowerCase());
  }
}
