import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-list-filter',
  templateUrl: './list-filter.component.html',
  styleUrls: ['./list-filter.component.scss']
})
export class ListFilterComponent implements OnInit {
  filterValue: string;
  @Output() filterChange = new EventEmitter<string>();
  constructor() { }

  ngOnInit() {
  }

}
