import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DragonsRoutingModule } from './dragons-routing.module';
import { DragonListComponent } from './containers/dragon-list/dragon-list.component';
import { DragonDetailsComponent } from './containers/dragon-details/dragon-details.component';
import { DragonEditComponent } from './containers/dragon-edit/dragon-edit.component';
import { DragonService } from './services/dragon.service';
import { ListComponent } from './components/list/list.component';
import { AngularMaterialModule } from 'src/app/shared/angular-material.module';
import { DragonListsResolver } from './resolvers/dragon-list.resolver';
import { DragonFormComponent } from './components/dragon-form/dragon-form.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { DragonResolver } from './resolvers/dragon.resolver';
import { DragonNewComponent } from './containers/dragon-new/dragon-new.component';
import { ListFilterComponent } from './components/list-filter/list-filter.component';
import { PreventUnsavedChangesGuard } from './guards/prevent-unsaved-changes.guard';

@NgModule({
  declarations: [
    DragonListComponent,
    DragonDetailsComponent,
    DragonEditComponent,
    ListComponent,
    DragonFormComponent,
    DragonNewComponent,
    ListFilterComponent
  ],
  imports: [CommonModule, ReactiveFormsModule, FormsModule, DragonsRoutingModule, AngularMaterialModule],
  providers: [
    DragonService,
    DragonListsResolver,
    DragonResolver,
    PreventUnsavedChangesGuard
  ]
})
export class DragonsModule {}
