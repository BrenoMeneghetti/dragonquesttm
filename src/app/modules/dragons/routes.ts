import { Routes } from '@angular/router';
import { AuthGuard } from 'src/app/shared/guards/auth.guard';
import { DragonDetailsComponent } from './containers/dragon-details/dragon-details.component';
import { DragonListComponent } from './containers/dragon-list/dragon-list.component';
import { DragonEditComponent } from './containers/dragon-edit/dragon-edit.component';
import { DragonListsResolver } from './resolvers/dragon-list.resolver';
import { DragonResolver } from './resolvers/dragon.resolver';
import { DragonNewComponent } from './containers/dragon-new/dragon-new.component';
import { PreventUnsavedChangesGuard } from './guards/prevent-unsaved-changes.guard';

export const dragonRoutes: Routes = [
  {
    path: 'dragons',
    canActivate: [AuthGuard],
    component: DragonListComponent,
    resolve: { dragonList: DragonListsResolver }
  },
  {
    path: '',
    runGuardsAndResolvers: 'always',
    canActivate: [AuthGuard],
    children: [
      {
        path: 'dragon/new',
        component: DragonNewComponent,
        canDeactivate: [PreventUnsavedChangesGuard]
      },
      {
        path: 'dragon/:id',
        component: DragonDetailsComponent,
        resolve: {dragon: DragonResolver}
      },
      {
        path: 'dragon/edit/:id',
        component: DragonEditComponent,
        resolve: {dragon: DragonResolver},
        canDeactivate: [PreventUnsavedChangesGuard]
      }
    ]
  }
];
