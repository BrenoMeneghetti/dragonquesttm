import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Dragon } from 'src/app/shared/models/dragon';
import { DragonService } from '../services/dragon.service';

@Injectable()
export class DragonResolver implements Resolve<Dragon> {
  constructor(private dragonService: DragonService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<Dragon> {
    return this.dragonService.getDragon(route.params.id).pipe(
      catchError(error => {
        this.router.navigate(['/']);
        return of(null);
      })
    );
  }
}
