import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Dragon } from 'src/app/shared/models/dragon';

@Injectable({
  providedIn: 'root'
})
export class DragonService {
  apiUrl = environment.dragonAPI + '/';
  constructor(private http: HttpClient ) {}

  getDragons(): Observable<object> {
    return this.http.get(this.apiUrl);
  }

  getDragon(id: number) {
    return this.http.get(this.apiUrl + id);
  }

  createDragon(dragon: Dragon) {
    return this.http.post(this.apiUrl, dragon);
  }

  editDragon(id: number, dragon: Dragon) {
    return this.http.put(this.apiUrl + id, dragon);
  }

  deleteDragon(id: number) {
    return this.http.delete(this.apiUrl + id);
  }
}
