import { Injectable } from '@angular/core';
import {
  CanDeactivate
} from '@angular/router';
import { DragonNewComponent } from '../containers/dragon-new/dragon-new.component';
import { DragonEditComponent } from '../containers/dragon-edit/dragon-edit.component';

@Injectable({
  providedIn: 'root'
})
export class PreventUnsavedChangesGuard
  implements CanDeactivate<DragonNewComponent | DragonEditComponent> {
  constructor() {}
  canDeactivate(component: DragonNewComponent | DragonEditComponent) {
    if (component.dragonForm && component.dragonForm.dragonForm.dirty) {
      return confirm(
        'Are you sure you want to continue? Any unsaved changes will be lost'
      );
    }
    return true;
  }
}
