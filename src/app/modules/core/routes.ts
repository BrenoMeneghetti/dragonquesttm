import { Routes } from '@angular/router';

import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { AuthGuard } from 'src/app/shared/guards/auth.guard';


export const coreRoutes: Routes = [
  { path: '', canActivate: [AuthGuard], component: HomeComponent },
  { path: 'login', component: LoginComponent },
  {
    path: '',
    runGuardsAndResolvers: 'always',
    canActivate: [AuthGuard],
    children: [

      // {
      //   path: 'members',
      //   component: MemberListComponent,
      //   resolve: { users: MemberListResolver }
      // },
      // {
      //   path: 'members/:id',
      //   component: MemberDetailComponent,
      //   resolve: { user: MemberDetailResolver }
      // },
      // {
      //   path: 'member/edit',
      //   component: MemberEditComponent,
      //   resolve: { user: MemberEditResolver },
      //   canDeactivate: [PreventUnsavedChangesGuard]
      // },
      // { path: 'messages', component: MessagesListComponent },
      // { path: 'lists', component: ListsComponent, resolve: {users: ListsResolver} }
    ]
  }
];
