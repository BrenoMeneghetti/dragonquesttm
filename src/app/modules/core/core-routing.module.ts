import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { coreRoutes } from './routes';



@NgModule({
  imports: [RouterModule.forChild(coreRoutes)],
  exports: [RouterModule]
})
export class CoreRoutingModule { }
