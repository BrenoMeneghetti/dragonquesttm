import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavComponent } from './components/nav/nav.component';
import { RouterModule } from '@angular/router';

import { LoginComponent } from './components/login/login.component';
import { ReactiveFormsModule } from '@angular/forms';
import { fakeBackendProvider } from './mockups/fake-backend.interceptor';
import { HttpClientModule } from '@angular/common/http';
import { AuthService } from './services/auth.service';
import { HomeComponent } from './components/home/home.component';
import { CoreRoutingModule } from './core-routing.module';
import { AngularMaterialModule } from 'src/app/shared/angular-material.module';
import { ConfirmationDialogComponent } from './components/confirmation-dialog/confirmation-dialog.component';
import { AlertService } from './services/alert.service';
import { LoadingIndicatorComponent } from './components/loading-indicator/loading-indicator.component';

@NgModule({
  declarations: [NavComponent, LoginComponent, HomeComponent, ConfirmationDialogComponent, LoadingIndicatorComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule,
    HttpClientModule,
    AngularMaterialModule,
    CoreRoutingModule
  ],
  exports: [
    NavComponent
  ],
  providers: [
    // providers used to create fake backend
    fakeBackendProvider,
    AuthService,
    AlertService
  ],
  entryComponents: [ConfirmationDialogComponent]
})
export class CoreModule { }
