import { Injectable } from '@angular/core';
import { MatDialog, MatSnackBar, MatDialogConfig } from '@angular/material';
import { ConfirmationDialogComponent } from '../components/confirmation-dialog/confirmation-dialog.component';

@Injectable({
  providedIn: 'root'
})
export class AlertService {
  constructor(private snackBar: MatSnackBar, public dialog: MatDialog) {}

  openConfirmDialog(title: string, description: string) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {
      title,
      description
    };
    const dialogRef = this.dialog.open(
      ConfirmationDialogComponent,
      dialogConfig
    );

    return dialogRef.afterClosed();
  }

  openSnackBar(message: string, action: string, duration: number = 2000) {
    const snackBarRef = this.snackBar.open(message, action, {
      duration
    });
    return snackBarRef.onAction();
  }
}
