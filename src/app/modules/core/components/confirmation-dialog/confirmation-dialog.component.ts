import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-confirmation-dialog',
  templateUrl: './confirmation-dialog.component.html',
  styleUrls: ['./confirmation-dialog.component.scss']
})
export class ConfirmationDialogComponent implements OnInit {
  description: string;
  title: string;

  constructor(@Inject(MAT_DIALOG_DATA) data) {
    this.description = data.description;
    this.title = data.title;
  }

  ngOnInit() {}
}
