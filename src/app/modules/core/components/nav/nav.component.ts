import { Component, OnInit, ViewChild } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { MatSidenav } from '@angular/material';
import { environment } from 'src/environments/environment';

interface MenuItem {
  icon?: string;
  route?: string;
  title?: string;
}

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {
  isMobile: boolean;
  appTitle: string;
  @ViewChild('drawer', { static: true }) public sidenav: MatSidenav;
  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(
      map(result => {
        this.isMobile = result.matches;
        return result.matches;
      })
    );

  menuItemList: MenuItem[] = [
    {
      icon: 'list',
      route: '/dragons',
      title: 'Dragon List'
    },
    {
      icon: 'add_circle',
      route: '/dragon/new',
      title: 'Add Dragon'
    }
  ];

  constructor(
    private breakpointObserver: BreakpointObserver,
    private authService: AuthService,
    private router: Router
  ) {
    this.appTitle = environment.appTitle;
  }

  ngOnInit(): void {}

  logout() {
    localStorage.removeItem('token');
    this.router.navigate(['/login']);
  }

  loggedIn() {
    return this.authService.loggedIn();
  }

  closeIfMobile() {
    if (this.isMobile) {
      this.sidenav.close();
    }
  }

  getUserFirstName() {
    return (
      this.authService.loggedIn() && this.authService.decodedToken.firstName
    );
  }
}
