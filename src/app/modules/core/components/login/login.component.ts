import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styles: [
    `
      .login-container {
        height: 100vh;
        display: grid;
        justify-items: center;
        align-items: center;
      }
      .login-card-container {
        width: 400px;
        // height: 230px;
        height: 320px;
        padding: 32px;
      }
    `
  ]
})
export class LoginComponent implements OnInit {
  loginForm = null;
  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private snackBar: MatSnackBar
  ) {
    this.loginForm = this.fb.group({
      username: [null, Validators.required],
      password: [null, Validators.required]
    });
  }

  ngOnInit() {}

  onSubmit() {
    this.authService.login(this.loginForm.value).subscribe(
      value => {
        this.router.navigate(['/']);
      },
      ({ error }) => {
        console.log(error.message);
        if (error && error.message) {
          this.openSnackBar(error.message, 'Close');
        } else {
          this.openSnackBar(
            'An error happened during login process. Please try again.',
            'Close'
          );
        }
      }
    );
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000
    });
  }
}
