import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  template: `
    <mat-card class="home-card">
      <mat-card-header>
        <mat-card-title class='home-title'>Welcome to the dragons' paradise!</mat-card-title>
      </mat-card-header>
      <mat-card-content>
        <img src='../../../../../assets/dragon-symbol.png' alt="Dragon image" />
      </mat-card-content>
    </mat-card>
  `,
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  constructor() {}

  ngOnInit() {
  }
}
